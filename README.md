# The code assessment submission #

The submission of the code assessment demonstrates the idea of implementing the API using a hard coded data source. In a real solution, it would include DbContext (if using Entity Framework) to describe / define the database schema and set up the communication with the database, along with, e.g. logger, auth, etc..

### Solution structure ###

The solution consists of four components, namely CodeAssessment.Api, CodeAssessment.Service, CodeAssessment.Data and CodeAssessment.Test.

* CodeAssessment.Api    
This is the entry component of this solution. Most of it is pretty much standard code. The sample data source and interface implementation are added to the DI container in Startup.cs. The API endpoint is defined in OrderController.cs. The controller method doesn't do much but just calling the service to handle the request and trying to catch exceptions.

* CodeAssessment.Service     
This library defines services that actually process requests and implement business logic that doesn't involve data access. It also defines business models that describe the entities involved in use cases.

* CodeAssessment.Data     
This is the data access component that directly communicates with the data source. It uses the respository pattern so that it's independent of any specific data source making it painless when switching to another data store. It also helps with unit testing the data access logic.      
Entities in this libary defines the schemas of data entities in the data store, while DTOs are used to pass data between the library itself and its consumers because we don't want it to depend on upper layers. In some cases, it's ok to make the business models truly global as a dependency across the entire solution including the data access component, but that's another topic to discuss.      
IDataSource describe what the data source can provide and the SampleDataSource provides the actual data for this solution. Since the SampleDataSource is a hard coded data source, the solution isn't using asynchronous programming.

* CodeAssessment.Test     
Unit tests for the Data, Service and Api controller components. The NSubstitute library is used to mock up dependencies.

### Assumptions ###

* The datastore holds the single source of truth in terms of UnitPrice of products because the UnitPrice in the 'Basket' could be tampered.

* The StartDate and EndDate of discounts and promos define the availability period. So the order's transaction date will be checked against the availability period to determine which discounts / promos are applicable.

* If multiple discounts are available to a product, the combined discount is applied by summing up each discount percentage instead of rolling one discount percentage on another.

* Points are calculated based on the applicable amount before discount is applied.

### How to run ###

This solution targets .NET 6 to benefit from the MaxBy() method in Entity Framework Core 6. To run the API, load the solution in Visual Studio, set CodeAssessment.Api as the startup project and press F5. This solution has swagger enabled, so once launched, the swagger page will appear and the only API endpoint "/Order" will be ready for trying out.

Unit tests can be run from Test Explorer.