﻿using CodeAssessment.Data.DataSources;
using CodeAssessment.Data.Entities;
using System;
using System.Collections.Generic;

namespace CodeAssessment.Test.DataSources
{
    public class TestDataSource : IDataSource
    {
        public List<DiscountPromotion> DiscountPromotions { get; set; }
        public List<DiscountPromotionProduct> DiscountPromotionProducts { get; set; }
        public List<PointsPromotion> PointsPromotions { get; set; }
        public List<Product> Products { get; set; }

        public TestDataSource()
        {
            DiscountPromotions = new List<DiscountPromotion>()
            {
                new DiscountPromotion()
                {
                    Id = "DP001",
                    Name = "Fuel Discount Promo",
                    StartDate = new DateTime(2020, 1, 1),
                    EndDate = new DateTime(2020, 2, 15),
                    DiscountPercent = 20
                },
                new DiscountPromotion()
                {
                    Id = "DP002",
                    Name = "Happy Promo",
                    StartDate = new DateTime(2020, 3, 2),
                    EndDate = new DateTime(2020, 3, 20),
                    DiscountPercent = 15
                }
            };

            DiscountPromotionProducts = new List<DiscountPromotionProduct>()
            {
                new DiscountPromotionProduct()
                {
                    DiscountPromotionId = "DP001",
                    ProductId = "PRD02"
                }
            };

            PointsPromotions = new List<PointsPromotion>()
            {
                new PointsPromotion()
                {
                    Id = "PP001",
                    Name = "New Year Promo",
                    StartDate = new DateTime(2020, 1, 1),
                    EndDate= new DateTime(2020, 1, 30),
                    Category = "Any",
                    PointsPerDollar = 2
                },
                new PointsPromotion()
                {
                    Id = "PP002",
                    Name = "Fuel Promo",
                    StartDate = new DateTime(2020, 2, 5),
                    EndDate= new DateTime(2020, 2, 15),
                    Category = "Fuel",
                    PointsPerDollar = 3
                },
                new PointsPromotion()
                {
                    Id = "PP003",
                    Name = "Shop Promo",
                    StartDate = new DateTime(2020, 3, 1),
                    EndDate= new DateTime(2020, 3, 20),
                    Category = "Shop",
                    PointsPerDollar = 4
                },
                new PointsPromotion()
                {
                    Id = "PP004",
                    Name = "Shop Promo 2",
                    StartDate = new DateTime(2020, 3, 1),
                    EndDate= new DateTime(2020, 3, 20),
                    Category = "Shop",
                    PointsPerDollar = 5
                }
            };

            Products = new List<Product>()
            {
                new Product()
                {
                    Id = "PRD01",
                    Name = "Vortex 95",
                    Category = "Fuel",
                    UnitPrice = 1.2m
                },
                new Product()
                {
                    Id = "PRD02",
                    Name = "Vortex 98",
                    Category = "Fuel",
                    UnitPrice = 1.3m
                },
                new Product()
                {
                    Id = "PRD03",
                    Name = "Diesel",
                    Category = "Fuel",
                    UnitPrice = 1.1m
                },
                new Product()
                {
                    Id = "PRD04",
                    Name = "Twix 55g",
                    Category = "Shop",
                    UnitPrice = 2.3m
                },
                new Product()
                {
                    Id = "PRD05",
                    Name = "Mars 72g",
                    Category = "Shop",
                    UnitPrice = 5.1m
                },
                new Product()
                {
                    Id = "PRD06",
                    Name = "SNICKERS 72G",
                    Category = "Shop",
                    UnitPrice = 3.4m
                },
                new Product()
                {
                    Id = "PRD07",
                    Name = "Bounty 3 63g",
                    Category = "Shop",
                    UnitPrice = 6.9m
                },
                new Product()
                {
                    Id = "PRD08",
                    Name = "Snickers 50g",
                    Category = "Shop",
                    UnitPrice = 4.0m
                },
                new Product()
                {
                    Id = "PRD09",
                    Name = "Dummy Product",
                    Category = "Other",
                    UnitPrice = 2.0m
                }
            };
        }
    }
}
