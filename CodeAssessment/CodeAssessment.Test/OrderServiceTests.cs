﻿using CodeAssessment.Data.Dtos;
using CodeAssessment.Data.Repositories;
using CodeAssessment.Service.Models;
using CodeAssessment.Service.Orders;
using CodeAssessment.Service.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;

namespace CodeAssessment.Test
{
    [TestClass]
    public class OrderServiceTests
    {
        private readonly IOrderRepository _repository;
        private readonly OrderService _orderService;

        public OrderServiceTests()
        {
            _repository = Substitute.For<IOrderRepository>();
            _orderService = new OrderService(_repository);
        }

        [TestMethod]
        public void GetOrderSummary_OK()
        {
            var transactionDate = new DateTime(2020, 1, 15);
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.NewGuid(),
                LoyaltyCard = "124356789",
                TransactionDate = "15-Apr-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "2.4",
                        Quantity = "3"
                    }
                }
            };
            _repository.GetOrderSummary(Arg.Any<DateTime>(), Arg.Any<IEnumerable<BasketItemDto>>()).Returns(new OrderSummaryDto());

            var orderSummary = _orderService.GetOrderSummary(orderSubmision);

            Assert.IsNotNull(orderSummary);
        }

        [TestMethod]
        public void Validaion_InvalidCustomerId()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "2.4",
                        Quantity = "3"
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidLoyaltyCard()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "2.4",
                        Quantity = "3"
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidTransactionDate()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.NewGuid(),
                LoyaltyCard = "124356789",
                TransactionDate = "30Feb2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "2.4",
                        Quantity = "3"
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidEmptyBasket()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = null
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });

            orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem> { }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidProductId()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "",
                        UnitPrice = "2.4",
                        Quantity = "3"
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidUnitPrice()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "",
                        Quantity = "3"
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }

        [TestMethod]
        public void Validaion_InvalidQuantity()
        {
            var orderSubmision = new OrderSubmission
            {
                CustomerId = Guid.Empty,
                LoyaltyCard = "124356789",
                TransactionDate = "30-Jan-2020",
                Basket = new List<OrderBasketItem>
                {
                    new OrderBasketItem()
                    {
                        ProductId = "PRD01",
                        UnitPrice = "2.4",
                        Quantity = ""
                    }
                }
            };

            Assert.ThrowsException<OrderValidationException>(() =>
            {
                _ = _orderService.GetOrderSummary(orderSubmision);
            });
        }
    }
}
