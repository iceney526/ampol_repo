﻿using CodeAssessment.Api.Controllers;
using CodeAssessment.Service.Models;
using CodeAssessment.Service.Orders;
using CodeAssessment.Service.Validators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;


namespace CodeAssessment.Test
{
    [TestClass]
    public class OrderControllerTests
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderService _orderService;
        private readonly OrderController _orderController;

        public OrderControllerTests()
        {
            _logger = Substitute.For<ILogger<OrderController>>();
            _orderService = Substitute.For<IOrderService>();
            _orderController = new OrderController(_logger, _orderService);
        }

        [TestMethod]
        public void Submit_OK()
        {
            var orderSubmission = new OrderSubmission();
            _orderService.GetOrderSummary(orderSubmission).Returns(new OrderSummary());

            var objectResult = (ObjectResult)_orderController.Submit(orderSubmission);

            _orderService.GetOrderSummary(orderSubmission).Received(1);
            Assert.AreEqual(200, objectResult.StatusCode);
        }

        [TestMethod]
        public void Submit_ValidationFailure()
        {
            var orderSubmission = new OrderSubmission();
            _orderService.GetOrderSummary(orderSubmission).Returns(x => throw new OrderValidationException("Validation failed.", new List<FluentValidation.Results.ValidationFailure>()));

            var objectResult = (ObjectResult)_orderController.Submit(orderSubmission);

            Assert.AreEqual(400, objectResult.StatusCode);
        }

        [TestMethod]
        public void Submit_GeneralFailure()
        {
            var orderSubmission = new OrderSubmission();
            _orderService.GetOrderSummary(orderSubmission).Returns(x => throw new Exception("Failed to get order summary."));

            var objectResult = (ObjectResult)_orderController.Submit(orderSubmission);

            Assert.AreEqual(500, objectResult.StatusCode);
        }
    }
}
