﻿using CodeAssessment.Data.DataSources;
using CodeAssessment.Data.Dtos;
using CodeAssessment.Data.Exceptions;
using CodeAssessment.Data.Repositories;
using CodeAssessment.Test.DataSources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CodeAssessment.Test
{
    [TestClass]
    public class OrderRepositoryTests
    {
        private readonly IDataSource _testDataSource;
        private readonly OrderRepository _orderRepository;

        public OrderRepositoryTests()
        {
            _testDataSource = new TestDataSource();
            _orderRepository = new OrderRepository(_testDataSource);
        }

        [TestMethod]
        public void EmptyBasket()
        {
            var orderSummary = _orderRepository.GetOrderSummary(DateTime.Today, null);
            Assert.IsNull(orderSummary);

            orderSummary = _orderRepository.GetOrderSummary(DateTime.Today, new List<BasketItemDto>());
            Assert.IsNull(orderSummary);
        }

        [TestMethod]
        public void NonExistingProducts()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD11",
                    Quantity = 2
                }
            };

            Assert.ThrowsException<OrderException>(() =>
            {
                var orderSummary = _orderRepository.GetOrderSummary(DateTime.Today, basketItems);
            });
        }

        [TestMethod]
        public void TotalAmount_OK()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD01",
                    Quantity = 2
                },
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 4
                },
            };

            var orderSummary = _orderRepository.GetOrderSummary(DateTime.Today, basketItems);

            Assert.AreEqual(7.6m, orderSummary.TotalAmount);
        }

        [TestMethod]
        public void Discount_AppliedOK()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 1, 15), basketItems);

            Assert.AreEqual(0.52m, orderSummary.DiscountApplied);
        }

        [TestMethod]
        public void GrandTotal_OK()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD01",
                    Quantity = 2
                },
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 4
                },
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 1, 15), basketItems);

            Assert.AreEqual(6.56m, orderSummary.GrandTotal);
        }

        [TestMethod]
        public void Discount_NonApplicableProduct()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD01",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 1, 15), basketItems);

            Assert.AreEqual(0, orderSummary.DiscountApplied);
        }

        [TestMethod]
        public void Discount_NonApplicableTransactionDate()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 2, 16), basketItems);

            Assert.AreEqual(0, orderSummary.DiscountApplied);
        }

        [TestMethod]
        public void PointsPromo_AppliedOk()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 2, 10), basketItems);

            Assert.AreEqual(6, orderSummary.PointsEarned);
        }

        [TestMethod]
        public void PointsPromo_FallbackToAny_AppliedOk()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 1, 15), basketItems);

            Assert.AreEqual(4, orderSummary.PointsEarned);
        }

        [TestMethod]
        public void PointsPromo_AnyCategory()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD09",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 1, 15), basketItems);

            Assert.AreEqual(8, orderSummary.PointsEarned);
        }

        [TestMethod]
        public void PointsPromo_NonApplicableTransactionDate()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD02",
                    Quantity = 2
                }
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 4, 1), basketItems);

            Assert.AreEqual(0, orderSummary.PointsEarned);
        }

        [TestMethod]
        public void PointsPromo_MultiplePromo()
        {
            var basketItems = new List<BasketItemDto>()
            {
                new BasketItemDto
                {
                    ProductId = "PRD01",
                    Quantity = 2
                },
                new BasketItemDto
                {
                    ProductId = "PRD04",
                    Quantity = 4
                },
            };

            var orderSummary = _orderRepository.GetOrderSummary(new DateTime(2020, 3, 15), basketItems);

            Assert.AreEqual(45, orderSummary.PointsEarned);
        }
    }
}