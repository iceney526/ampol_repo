﻿using CodeAssessment.Service.Models;
using CodeAssessment.Service.Orders;
using CodeAssessment.Service.Validators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace CodeAssessment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderService _orderService;

        public OrderController(ILogger<OrderController> logger, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
        }

        [HttpPost]
        public IActionResult Submit(OrderSubmission request)
        {
            try
            {
                var orderSummary = _orderService.GetOrderSummary(request);
                return Ok(orderSummary);
            }
            catch (OrderValidationException vex)
            {
                _logger.LogError(vex, "Found validation errors.");
                return BadRequest(new 
                { 
                    vex.Message, 
                    Errors = vex.ValidationErrors.Select(x => x.ErrorMessage).ToList() 
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Errors occurred when computing order summary.");
                return StatusCode(500, "Failed to get order summary.");
            }
        }
    }
}
