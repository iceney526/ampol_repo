﻿using System;

namespace CodeAssessment.Data.Exceptions
{
    public class OrderException : Exception
    {
        public OrderException(string message)
            : base(message) 
        { }
    }
}
