﻿using CodeAssessment.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Data.DataSources
{
    public interface IDataSource
    {
        List<DiscountPromotion> DiscountPromotions { get; set; }
        List<DiscountPromotionProduct> DiscountPromotionProducts { get; set; }
        List<PointsPromotion> PointsPromotions { get; set; }
        List<Product> Products { get; set; }
    }
}
