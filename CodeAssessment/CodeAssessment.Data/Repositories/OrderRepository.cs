﻿using CodeAssessment.Data.DataSources;
using CodeAssessment.Data.Dtos;
using CodeAssessment.Data.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeAssessment.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private IDataSource _dataSource;

        public OrderRepository(IDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public OrderSummaryDto GetOrderSummary(DateTime transactionDate, IEnumerable<BasketItemDto> basketItems)
        {
            if (basketItems == null || !basketItems.Any())
            {
                return null;
            }

            var items = basketItems.ToList();
            var orderProducts = (from p in _dataSource.Products
                                 join i in items on p.Id equals i.ProductId
                                 select new
                                 {
                                     Product = p,
                                     i.Quantity
                                 }).ToList();

            if (orderProducts.Count != items.Count)
            {
                throw new OrderException("Some products couldn't been found in database.");
            }

            //
            // Compute total amount
            //
            var totalAmount = orderProducts.Sum(x => x.Product.UnitPrice * x.Quantity);

            //
            // Compute discounts
            //
            var discounts = (from op in orderProducts
                             join dpp in _dataSource.DiscountPromotionProducts on op.Product.Id equals dpp.ProductId
                             join dp in _dataSource.DiscountPromotions.Where(x => x.StartDate <= transactionDate && transactionDate <= x.EndDate) on dpp.DiscountPromotionId equals dp.Id
                             select new
                             {
                                 op.Product.Id,
                                 op.Quantity,
                                 op.Product.UnitPrice,
                                 dp.DiscountPercent
                             }).ToList();
            // Note: if multiple discounts are available to a product, the combined discount is applied by summing up each discount percentage instead of rolling one discount percentage on another.
            var discountsApplied = discounts.Sum(x => x.Quantity * x.UnitPrice * x.DiscountPercent / 100);

            //
            // Compute points
            //
            var activePointsPromos = _dataSource.PointsPromotions.Where(x => x.StartDate <= transactionDate && transactionDate <= x.EndDate).ToList();
            var orderPointsPromos = orderProducts.Select(x => new
                                                      {
                                                          ProductId = x.Product.Id,
                                                          Amount = x.Product.UnitPrice * x.Quantity,
                                                          PointsPromo = activePointsPromos.Where(app => app.Category == x.Product.Category || app.Category == "Any")
                                                                                          .MaxBy(x => x.PointsPerDollar)
                                                      })
                                                 .Where(x => x.PointsPromo != null)
                                                 .ToList();

            var pointsEarned = orderPointsPromos.Any() 
                               ? orderPointsPromos.GroupBy(x => x.PointsPromo.Id)
                                                  .Max(g => g.Sum(v => (int)v.Amount * v.PointsPromo.PointsPerDollar)) // Only apply one points promo that has the most points.
                               : 0;

            //
            // Return the result
            //
            var orderSummaryDto = new OrderSummaryDto()
            {
                TotalAmount = totalAmount,
                DiscountApplied = discountsApplied,
                GrandTotal = totalAmount - discountsApplied,
                PointsEarned = pointsEarned,
            };

            return orderSummaryDto;
        }
    }
}
