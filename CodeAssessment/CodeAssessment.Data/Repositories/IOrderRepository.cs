﻿using CodeAssessment.Data.Dtos;
using System;
using System.Collections.Generic;

namespace CodeAssessment.Data.Repositories
{
    public interface IOrderRepository
    {
        OrderSummaryDto GetOrderSummary(DateTime transactionDate, IEnumerable<BasketItemDto> basketItems);
    }
}
