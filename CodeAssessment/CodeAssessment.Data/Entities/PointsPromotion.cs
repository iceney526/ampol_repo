﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Data.Entities
{
    public class PointsPromotion
    {
        public string Id { get;set; }
        public string Name { get;set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Category { get; set; }
        public int PointsPerDollar { get; set; }
    }
}
