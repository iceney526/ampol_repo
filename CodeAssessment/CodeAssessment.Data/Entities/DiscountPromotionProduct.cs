﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Data.Entities
{
    public class DiscountPromotionProduct
    {
        public string DiscountPromotionId { get; set; }
        public string ProductId { get; set; }
    }
}
