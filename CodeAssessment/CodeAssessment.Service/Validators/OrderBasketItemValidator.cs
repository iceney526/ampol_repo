﻿using CodeAssessment.Service.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Service.Validators
{
    public class OrderBasketItemValidator : AbstractValidator<OrderBasketItem>
    {
        public OrderBasketItemValidator()
        {
            RuleFor(x => x.ProductId).NotEmpty();
            RuleFor(x => x.UnitPrice).NotEmpty().Matches(@"^\d+\.?\d*$");
            RuleFor(x => x.Quantity).NotEmpty().Matches(@"^\d+$");
        }
    }
}
