﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Service.Validators
{
    public class OrderValidationException : Exception
    {
        public string Message { get; }
        public List<ValidationFailure> ValidationErrors { get; }

        public OrderValidationException(string message, List<ValidationFailure> validationErrors)
        {
            Message = message;
            ValidationErrors = validationErrors;
        }
    }
}
