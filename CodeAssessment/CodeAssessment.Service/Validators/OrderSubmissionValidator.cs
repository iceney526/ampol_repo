﻿using CodeAssessment.Service.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssessment.Service.Validators
{
    public class OrderSubmissionValidator : AbstractValidator<OrderSubmission>
    {
        public OrderSubmissionValidator()
        {
            RuleFor(x => x.CustomerId).NotEmpty();
            RuleFor(x => x.LoyaltyCard).NotEmpty();
            RuleFor(x => x.TransactionDate).Must(x =>
            {
                DateTime dt;
                return DateTime.TryParse(x, out dt);
            }).WithMessage("Invalid transaction date.");
            RuleFor(x => x.Basket).Must(x => x != null && x.Any()).WithMessage("Basket can't be empty");
            RuleForEach(x => x.Basket).NotNull().SetValidator(new OrderBasketItemValidator());
        }
    }
}
