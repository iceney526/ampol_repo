namespace CodeAssessment.Service.Models
{
    public class OrderBasketItem
    {
        public string ProductId { get; set; } = string.Empty;
        public string UnitPrice { get; set; } = string.Empty;
        public string Quantity { get; set; } = string.Empty;
    }
}