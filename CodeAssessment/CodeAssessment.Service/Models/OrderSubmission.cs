namespace CodeAssessment.Service.Models
{
    public class OrderSubmission
    {
        public Guid CustomerId { get; set; } = Guid.Empty;
        public string LoyaltyCard { get; set; } = string.Empty;
        public string TransactionDate { get; set; } = string.Empty;
        public List<OrderBasketItem>? Basket { get; set; }
    }
}