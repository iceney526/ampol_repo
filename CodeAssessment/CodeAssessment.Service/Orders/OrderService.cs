﻿using CodeAssessment.Data.Dtos;
using CodeAssessment.Data.Repositories;
using CodeAssessment.Service.Models;
using CodeAssessment.Service.Validators;

namespace CodeAssessment.Service.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public OrderSummary GetOrderSummary(OrderSubmission orderSubmission)
        {
            var orderSubmissionValidator = new OrderSubmissionValidator();
            var validationResult = orderSubmissionValidator.Validate(orderSubmission);

            if (validationResult.IsValid)
            {
                var orderSummaryDto = _orderRepository.GetOrderSummary(
                    DateTime.Parse(orderSubmission.TransactionDate),
                    orderSubmission.Basket.Select(x => new BasketItemDto
                    {
                        ProductId = x.ProductId,
                        Quantity = int.Parse(x.Quantity),
                    }));

                var orderSummary = new OrderSummary
                {
                    CustomerId = orderSubmission.CustomerId,
                    LoyaltyCard = orderSubmission.LoyaltyCard,
                    TransactionDate = orderSubmission.TransactionDate,
                    TotalAmount = $"{orderSummaryDto.TotalAmount:N2}",
                    DiscountApplied = $"{orderSummaryDto.DiscountApplied:N2}",
                    GrandTotal = $"{orderSummaryDto.GrandTotal:N2}",
                    PointsEarned = orderSummaryDto.PointsEarned.ToString()
                };

                return orderSummary;
            }
            else
            {
                throw new OrderValidationException("Order validation failed.", validationResult.Errors);
            }
        }
    }
}
