﻿using CodeAssessment.Service.Models;

namespace CodeAssessment.Service.Orders
{
    public interface IOrderService
    {
        OrderSummary GetOrderSummary(OrderSubmission orderSubmission);
    }
}
